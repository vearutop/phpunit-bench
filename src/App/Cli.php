<?php

namespace PHPUnitBenchmark\App;


use PHPUnitBenchmark\App\Command\CaseMatrix;
use PHPUnitBenchmark\App\Command\Compare;
use PHPUnitBenchmark\App\Command\StoreResult;
use Yaoi\Command\Application;
use Yaoi\Command\Definition;

class Cli extends Application
{
    public $compare;
    public $caseMatrix;

    /**
     * @param Definition $definition
     * @param \stdClass|static $commandDefinitions
     */
    static function setUpCommands(Definition $definition, $commandDefinitions)
    {
        $definition->version = 'v0.0.0';
        $definition->name = 'phpunit-bench';
        $definition->description = 'PHPUnit benchmark helper app';

        $commandDefinitions->compare = Compare::definition();
        $commandDefinitions->caseMatrix = CaseMatrix::definition();
    }

}