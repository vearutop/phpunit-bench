<?php

namespace PHPUnitBenchmark\App\Command;

use PHPUnitBenchmarkData\Suite;
use Swaggest\CodeBuilder\TableRenderer;
use Yaoi\Command;
use Yaoi\Command\Definition;
use Yaoi\Io\Content\Rows;

class CaseMatrix extends Basic
{
    public $report;
    public $output;
    public $format;
    public $dataProviderSeparator;
    public $order;
    public $desc;
    public $filter;
    public $stripFailed;
    public $totalOnly;


    const FORMAT_CLI = 'cli';
    const FORMAT_CSV = 'csv';
    const FORMAT_TSV = 'tsv';
    const FORMAT_MD = 'md';

    /**
     * @param Definition $definition
     * @param \stdClass|static $options
     */
    static function setUpDefinition(Definition $definition, $options)
    {
        $options->report = Command\Option::create()->setType()->setIsUnnamed()->setIsRequired()
            ->setDescription('Path to report');
        $options->format = Command\Option::create()->setEnum(self::FORMAT_MD)
            ->setDescription('Output format');
        $options->dataProviderSeparator = Command\Option::create()->setType()
            ->setDescription('Expand data provider groups, test name <value> result name');
        $options->order = Command\Option::create()->setType()
            ->setDescription('Order result table by column');
        $options->desc = Command\Option::create()
            ->setDescription('Order descending');
        $options->filter = Command\Option::create()->setType()
            ->setDescription('Filter by test name regex');
        $options->stripFailed = Command\Option::create()
            ->setDescription('Remove failed tests from table (common passed only)');
        $options->totalOnly = Command\Option::create()
            ->setDescription('Show only total results, skip details');

    }

    public function performAction()
    {
        $report = Suite::import(json_decode(file_get_contents($this->report)));

        $resultMatrix = array();

        $testCaseNames = array_keys($report->testCases);
        $head = $this->findCommonHead($testCaseNames);
        $tail = $this->findCommonTail($testCaseNames);

        $testCaseNames = array_keys($report->testCases);
        foreach ($testCaseNames as $i => $testCaseName) {
            $testCaseNames[$i] = substr($testCaseName, strlen($head), -strlen($tail));
        }


        foreach ($report->testCases as $testCaseName => $testCase) {
            $testCaseName = substr($testCaseName, strlen($head), -strlen($tail));
            foreach ($testCase->tests as $testName => $test) {
                $testFailed = array();

                foreach ($test->results as $resultName => $result) {
                    if ($this->dataProviderSeparator) {
                        list($dataTest, $dataName) = explode($this->dataProviderSeparator, $resultName, 2);
                    } else {
                        $dataTest = 'default';
                    }

                    if ($result->timeSpent === null) {
                        $testFailed[$dataTest] = true;
                    }
                }

                foreach ($test->results as $resultName => $result) {
                    if ($this->dataProviderSeparator) {
                        list($dataTest, $dataName) = explode($this->dataProviderSeparator, $resultName, 2);
                        $resultKey = $testName . ':' . $dataTest;
                    } else {
                        $dataTest = 'default';
                        $resultKey = $testName . ':' . $resultName;
                    }
                    if ($this->filter) {
                        if (!preg_match('/' . $this->filter . '/i', $resultKey)) {
                            continue;
                        }
                    }

                    if (!isset($resultMatrix[$resultKey])) {
                        $resultMatrix[$resultKey] = array();
                    }

                    if (!isset($testFailed[$dataTest])) {
                        if (!isset($resultMatrix [$resultKey][$testCaseName])) {
                            $resultMatrix [$resultKey][$testCaseName] = $result->timeSpent;
                        } else {
                            $resultMatrix [$resultKey][$testCaseName] += $result->timeSpent;
                        }
                    }
                }
            }
        }

        $resultTable = array();
        $totalCommonPassedTime = array_combine($testCaseNames, array_fill(0, count($testCaseNames), 0));
        $totalWithFailedTime = array_combine($testCaseNames, array_fill(0, count($testCaseNames), 0));

        $passed = $totalCommonPassedTime;
        $totalTests = 0;
        foreach ($resultMatrix as $test => $result) {
            $min = $result ? min($result) : null;
            $row = array('Test' => $test);
            $skipTotal = false;
            $totalTests++;
            foreach ($testCaseNames as $testCaseName) {
                if (isset($result[$testCaseName])) {
                    $row[$testCaseName] = round(100 * $result[$testCaseName] / $min, 2);
                    $passed[$testCaseName]++;
                } else {
                    $skipTotal = true;
                    $row[$testCaseName] = '-';
                }
            }
            foreach ($result as $testCaseName => $value) {
                $totalWithFailedTime[$testCaseName] += $value;
            }
            if (!$skipTotal) {
                foreach ($result as $testCaseName => $value) {
                    $totalCommonPassedTime[$testCaseName] += $value;
                }
            }
            if (!($this->stripFailed && $skipTotal)) {
                $resultTable[] = $row;
            }
        }
        if ($this->order) {
            usort($resultTable, function ($a, $b) {
                return $this->desc
                    ? $a[$this->order] < $b[$this->order]
                    : $a[$this->order] > $b[$this->order];
            });
        }

        if ($this->totalOnly) {
            $resultTable = array();
        }

        $totalCommonPassedTime = $this->normalizeArray($totalCommonPassedTime);
        $totalCommonPassedTime['Test'] = 'Total time for common passed';
        $resultTable[] = $totalCommonPassedTime;

        $totalWithFailedTime = $this->normalizeArray($totalWithFailedTime);
        $totalWithFailedTime['Test'] = 'Total time for all tests (with failed)';
        $resultTable[] = $totalWithFailedTime;

        $passedCnt = $passed;
        $passedCnt['Test'] = 'Tests passed (total: ' . $totalTests . ')';
        $resultTable[] = $passedCnt;

        $passed['__total'] = $totalTests;
        $passed = $this->normalizeArray($passed, false);
        unset($passed['__total']);
        $passed['Test'] = 'Tests passed, %';
        $resultTable[] = $passed;

        if ($this->format) {
            if ($this->format === self::FORMAT_MD) {
                $mdTable = (string)TableRenderer::create(new \ArrayIterator($resultTable))
                    ->setColDelimiter('|')
                    ->setHeadRowDelimiter('-')
                    ->setOutlineVertical(true)
                    ->setShowHeader();
                $this->response->addContent($mdTable);
            }
        } else {
            $this->response->addContent(new Rows(new \ArrayIterator(array_values($resultTable))));
        }

    }


    private function normalizeArray($map, $byMin = true)
    {
        $norm = $byMin ? min($map) : max($map);
        if ($norm) {
            foreach ($map as $case => $value) {
                $map[$case] = round(100 * $value / $norm, 2);
            }
        }
        return $map;
    }

    private function findCommonHead(array $names)
    {
        $result = '';
        $sampleName = array_pop($names);
        for ($i = 0; $i < strlen($sampleName); ++$i) {
            $different = false;
            foreach ($names as $name) {
                if ($name[$i] !== $sampleName[$i]) {
                    $different = true;
                    break;
                }
            }
            if ($different) {
                break;
            } else {
                $result .= $sampleName[$i];
            }
        }
        return $result;
    }

    private function findCommonTail(array $names)
    {
        $result = '';
        $sampleName = array_pop($names);
        for ($i = 1; $i <= strlen($sampleName); ++$i) {
            $different = false;
            $check = substr($sampleName, -$i);
            foreach ($names as $name) {
                if ($check !== substr($name, -$i)) {
                    $different = true;
                    break;
                }
            }
            if ($different) {
                break;
            } else {
                $result = $check;
            }
        }
        return $result;
    }

}