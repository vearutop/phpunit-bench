<?php

namespace PHPUnitBenchmark\App\Command;

use PHPUnitBenchmarkData\Suite;
use Yaoi\Command;
use Yaoi\Command\Definition;
use Yaoi\Io\Content\Error;
use Yaoi\Io\Content\Rows;
use Yaoi\Io\Content\Success;

class Compare extends Basic
{
    public $reports;
    public $maxPerfDrop = 20;
    public $maxPerfImprove = 20;

    /**
     * @param Definition $definition
     * @param \stdClass|static $options
     */
    static function setUpDefinition(Definition $definition, $options)
    {
        $options->reports = Command\Option::create()
            ->setIsUnnamed()
            ->setIsVariadic()
            ->setIsRequired(true)
            ->setDescription('List of result json files');

        $options->maxPerfDrop = Command\Option::create()->setType()
            ->setDescription('Maximum allowed performance drop, percent to base');

        $options->maxPerfImprove = Command\Option::create()->setType()
            ->setDescription('Maximum allowed performance improve, percent to base');
    }

    public function performAction()
    {
        if (count($this->reports) < 2) {
            $this->response->error("At least two reports required");
        }

        $baseSuite = Suite::loadFromJsonFile($this->reports[0]);
        unset($this->reports[0]);

        $resultTable = array();

        foreach ($this->reports as $report) {
            $suite = Suite::loadFromJsonFile($report);

            $suiteRes = array(
                'Name' => 'Total',
                'Base' => 0,
                'Updated' => 0,
            );
            $resultTable['Total'] = &$suiteRes;


            foreach ($suite->testCases as $testCaseName => $testCase) {
                $testCaseRes = array(
                    'Name' => $testCaseName,
                    'Base' => 0,
                    'Updated' => 0,
                );
                $resultTable[$testCaseName] = &$testCaseRes;
                $testCount = 0;

                foreach ($testCase->tests as $testName => $test) {
                    $testRes = array(
                        'Name' => ' ' . $testName,
                        'Base' => 0,
                        'Updated' => 0,
                    );
                    $resultTable[$testCaseName . ':' . $testName] = &$testRes;
                    $count = 0;

                    foreach ($test->results as $dataName => $result) {
                        $key = $testCaseName . ':' . $testName . ':' . $dataName;

                        if (isset($baseSuite
                                ->testCases[$testCaseName]
                                ->tests[$testName]
                                ->results[$dataName])) {
                            $baseResult = $baseSuite
                                ->testCases[$testCaseName]
                                ->tests[$testName]
                                ->results[$dataName];

                            if ($baseResult->iterations === null || $result->iterations === null) {
                                continue;
                            }

                            $basePerf = $baseResult->timeSpent / ($baseResult->iterations * $baseSuite->hostPerformanceIndex);
                            $perf = $result->timeSpent / ($result->iterations * $suite->hostPerformanceIndex);

                            $testRes['Base'] += $basePerf;
                            $testRes['Updated'] += $perf;

                            $normalizedPerf = round(100 * ($perf / $basePerf - 1), 2); // in percent
                            if ($normalizedPerf < 0 && $this->maxPerfImprove && $normalizedPerf > -$this->maxPerfImprove) {
                                continue;
                            }
                            if ($normalizedPerf > 0 && $this->maxPerfDrop && $normalizedPerf < $this->maxPerfDrop) {
                                continue;
                            }
                            $normalizedPerf = $normalizedPerf > 0 ? new Error($normalizedPerf) : new Success($normalizedPerf);

                            if ($dataName !== 'unnamed') {
                                $resultTable[$key] = array(
                                    'Name' => '  ' . substr($dataName, 0, 380),
                                    'Base' => $basePerf,
                                    'Updated' => $perf,
                                    'Delta' => $normalizedPerf,
                                );
                            }
                            $count++;
                        }
                    }
                    $normalizedPerf = round(100 * ($testRes['Updated'] / $testRes['Base'] - 1), 2); // in percent
                    $testRes['Delta'] = $normalizedPerf > 0 ? new Error($normalizedPerf) : new Success($normalizedPerf);

                    if (0 === $count) {
                        unset($resultTable[$testCaseName . ':' . $testName]);
                    } else {
                        $testCount++;
                    }

                    $testCaseRes['Base'] += $testRes['Base'];
                    $testCaseRes['Updated'] += $testRes['Updated'];
                    unset($testRes);
                }
                $normalizedPerf = round(100 * ($testCaseRes['Updated'] / $testCaseRes['Base'] - 1), 2); // in percent
                $testCaseRes['Delta'] = $normalizedPerf > 0 ? new Error($normalizedPerf) : new Success($normalizedPerf);

                if (0 === $testCount) {
                    unset($resultTable[$testCaseName]);
                }

                $suiteRes['Base'] += $testCaseRes['Base'];
                $suiteRes['Updated'] += $testCaseRes['Updated'];
                unset($testCaseRes);
            }
            $normalizedPerf = round(100 * ($suiteRes['Updated'] / $suiteRes['Base'] - 1), 2); // in percent
            $suiteRes['Delta'] = $normalizedPerf > 0 ? new Error($normalizedPerf) : new Success($normalizedPerf);
        }

        ini_set('precision', 5);
        $this->response->addContent(new Rows(new \ArrayIterator(array_values($resultTable))));
    }

}